# whatsapp-bot

HOW TO (CL-Version):

    In the "users.txt" put the users(or groups) you want to recieve your messages.
    Each user(or group) goes into his own line.

    In the "times-and-texts.csv" put the times in the left collum and the texts in the right collum.
    The texts will then be sent at the given time.

    By starting the script, you have to scan the whatsapp-web QR-code.
    (thats just how whatsapp-web works)
    
HOW TO (GUI version):
    
    No secondary files needed (exept chromedriver).
    
    Enter name, date and text into the fields then press "add" and then "run".
    
    This still contains a few bugs ( :D ).


WARNING:

     Neither script has an exit function yet, so be sure to know how to cancel a process!
