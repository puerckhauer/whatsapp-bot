#!/usr/bin/env python3

from selenium import webdriver
import time as t
from selenium.webdriver.common.keys import Keys
from datetime import datetime
from tkinter import *
from tkinter import filedialog


entries = []


def check_time_format(time):
    if len(time) != 5:
        return False
    if time[2] != ':':
        return False
    hours, minutes = time.split(':')
    if not hours.isdigit() or not minutes.isdigit():
        return False
    hours = int(hours)
    minutes = int(minutes)
    print(type(hours))
    if hours < 0 or hours > 24 or minutes < 0 or minutes > 59:
        return False
    return True


def update_entries():
    entry_label.config(text="")
    for en in entries:
        entry_label.config(
            text=entry_label.cget("text") + "\n" + en.user + "\t" + en.time + "\t" + en.text)


def add_entry():
    if (user_input.get().strip() != "") & (time_input.get().strip() != "") & (text_input.get().strip() != ""):
        if check_time_format(time_input.get().strip()):
            entry = SpamEntry(user_input.get(), time_input.get(), text_input.get())
            entries.append(entry)
            update_entries()
        else:
            error_window = Tk()
            error_window.geometry("200x50")
            error_window.title("Error")
            error_label = Label(error_window, text="invalid time format")
            error_label.place(x=10, y=10)


def add_file_entry():
    file = filedialog.askopenfilename(initialdir="", title="select csv-file", filetypes=(("csv files", "*.csv"), ("all files", "*.*")))
    c = 0
    with open(file, 'r') as f:
        for line in f:
            if c == 0:
                split = line.split(";")
                user = split[0][3:]
                time = split[1]
                text = split[2].rstrip()
                entry = SpamEntry(user, time, text)
                entries.append(entry)
                c += 1
            else:
                split = line.split(";")
                user = split[0]
                time = split[1]
                text = split[2].rstrip()
                entry = SpamEntry(user, time, text)
                entries.append(entry)
    update_entries()


def write(chrome_browser):
    time = str(datetime.now().time())[:5]
    for e in entries:
        if e.time == time:
            search_box = chrome_browser.find_element_by_xpath(
                '/html/body/div[1]/div/div/div[3]/div/div[1]/div/label/div/div[2]')
            search_box.send_keys(e.user)
            search_box.send_keys(Keys.ENTER)
            message_box = chrome_browser.find_element_by_xpath(
                '/html/body/div[1]/div/div/div[4]/div/footer/div[1]/div[2]/div/div[2]')
            message_box.send_keys(e.text)
            message_box.send_keys(Keys.ENTER)

            entries.remove(e)
            update_entries()
    if len(entries) != 0:
        t.sleep(60)
        write(chrome_browser)
    else:
        chrome_browser.close()


def run():
    chrome_browser = webdriver.Chrome()
    chrome_browser.get('https://web.whatsapp.com/')
    t.sleep(10)
    write(chrome_browser)


class SpamEntry:
    """this class contains the entries that have to be executed"""
    def __init__(self, user="", time="", text=""):
        self.user = user
        self.time = time
        self.text = text


window = Tk()
window.geometry("600x600")
window.title("WhatsApp Bot")

run_button = Button(window, text="run", command=run)

file_input_button = Button(window, text="import from file", command=add_file_entry)

user_input_label = Label(window, text="recipient:")
user_input = Entry(window, bd=2, width=15)
time_input_label = Label(window, text="time:")
time_input = Entry(window, bd=2, width=10)
text_input_label = Label(window, text="text:")
text_input = Entry(window, bd=2, width=50)
add_button = Button(window, text="add", command=add_entry)

entry_label = Label(window, text="")

user_input_label.place(x=10, y=20)
user_input.place(x=10, y=50)
time_input_label.place(x=110, y=20)
time_input.place(x=110, y=50)
text_input_label.place(x=180, y=20)
text_input.place(x=180, y=50)
add_button.place(x=500, y=49, height=20)
entry_label.place(x=10, y=70)
run_button.place(x=550, y=25)
file_input_button.place(x=435, y=10)

window.mainloop()
