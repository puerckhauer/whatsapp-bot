#!/usr/bin/env python3
from selenium import webdriver
import time as t
from selenium.webdriver.common.keys import Keys
from datetime import datetime
import argparse as ap

parser = ap.ArgumentParser()
parser.add_argument('-users', required=True)
parser.add_argument('-texts', required=True)
args = parser.parse_args()

users_file = args.users
times_and_texts_file = args.texts

users = []
with open(users_file, 'r') as f:
    for line in f:
        users.append(line.rstrip())

times_and_texts = {}
c = 0
with open(times_and_texts_file, 'r') as f:
    for line in f:
        if c == 0:
            split = line.split(";")
            time = split[0][3:]
            text = split[1].rstrip()
            times_and_texts[time] = text
            c += 1
        else:
            split = line.split(";")
            time = split[0]
            text = split[1].rstrip()
            times_and_texts[time] = text

chrome_browser = webdriver.Chrome()
chrome_browser.get('https://web.whatsapp.com/')
t.sleep(10)

while True:
    time = str(datetime.now().time())[:5]
    for key in times_and_texts:
        if time == key:
            for user in users:
                search_box = chrome_browser.find_element_by_xpath('/html/body/div[1]/div/div/div[3]/div/div[1]/div/label/div/div[2]')
                search_box.send_keys(user)
                search_box.send_keys(Keys.ENTER)
                message_box = chrome_browser.find_element_by_xpath('/html/body/div[1]/div/div/div[4]/div/footer/div[1]/div[2]/div/div[2]')
                message_box.send_keys(times_and_texts[key])
                message_box.send_keys(Keys.ENTER)
    t.sleep(60)
